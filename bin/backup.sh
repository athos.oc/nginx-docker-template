#!/bin/bash
# cesar@superadmin.es
# 00 02 * * * /bin/bash -c /usr/local/sbin/backup 

set -e

dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd ${dir}
CWD=$(pwd)


_running() {
  local container="$1"
  docker ps -a -q --filter name="^/${container}$" --filter status="running"
}

# limpieza.
/usr/bin/find /backup \( -name "*tar*" -o -name "*sql*" \) -type f -ctime +3 -exec rm {} \;

# backup de la base de datos.
if [ -d /var/lib/mysql ] && [[ -n $(_running "mysql") ]]; then
	docker exec -i mysql sh -c 'mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} --max_allowed_packet=2G --opt --all-databases --force' > /backup/$(hostname).dbdump.$(date +%Y%m%d%H%M).sql
fi

# backup de los ficheros.
[ -d /var/www/html  ] && /bin/tar -zcvf /backup/$(hostname).html.$(date +%Y%m%d%H%M).tar.gz /var/www/html

exit 0

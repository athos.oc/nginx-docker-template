#!/bin/bash
# cesar@superadmin.es

set -e

dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd ${dir}
CWD=$(pwd)


# Importar scripts sql en mysql-import-files.
for i in $( find "$(cd ../mysql-import-files; pwd)" -type f -name "*sql" | sort);do
  # Tendre que separar por los dos proyectos
  #cat $i | docker exec -i mysql sh -c "mysql -u${WORDPRESS_DB_USER} -p${WORDPRESS_DB_PASSWORD} ${WORDPRESS_DB_NAME}"  
  cat $i | docker exec -i mysql sh -c "mysql -uroot -p${MYSQL_ROOT_PASSWORD}"
done

  